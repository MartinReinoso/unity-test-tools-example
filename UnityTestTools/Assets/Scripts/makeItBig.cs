﻿using UnityEngine;
using System.Collections;


public class makeItBig : MonoBehaviour {

	// Script attached to a Power up to increase the size by 2


	void OnTriggerEnter(Collider other) {
		if (other.transform.name == "Plane" ) {
			return;
		}
		if (
			other.transform.name == "CapsuleNoPowerUp") {
			return;
		}
		Vector3 scale = other.transform.localScale;
		other.transform.localScale = scale * 2; // Comment this line as an explample
	}
}
