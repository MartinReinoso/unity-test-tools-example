# README #

### What is this repository for? ###

* Is an example of using unity test tools (https://www.assetstore.unity3d.com/en/#!/content/13802)
* Version 1
* Ideas used from : 
https://unity3d.com/learn/tutorials/topics/production/unity-test-tools


### How do I test? ###

* Open the project folder (UnityTestTools) with Unity3D
* in Assets, Open the Scene test

![Screen Shot 2016-08-25 at 8.19.12 PM.png](https://bitbucket.org/repo/6L7kxe/images/3153779594-Screen%20Shot%202016-08-25%20at%208.19.12%20PM.png)

* Do NOT click the Play button in the top, this is not a game scene but a test one.
* In the top menu click on Unity Test Tools > Integration Test Runner

![Screen Shot 2016-08-25 at 8.23.03 PM.png](https://bitbucket.org/repo/6L7kxe/images/3482216302-Screen%20Shot%202016-08-25%20at%208.23.03%20PM.png)

* In the New window click on Run All

![Screen Shot 2016-08-25 at 8.23.50 PM.png](https://bitbucket.org/repo/6L7kxe/images/3860382064-Screen%20Shot%202016-08-25%20at%208.23.50%20PM.png)


### Tips for understanding ###

In the Hierarchy you can find the Test powerUp and No powerUp

**Test powerUp**

This test has 2 players, one powerUp and a plane
The test looks into make a player big with the power up.
The power up should not make the plane big.

Check with the inspector the properties of the objects Test powerUp

* Did you notice that Succeed on assertions is check?
* Did you notice the time out? We should complete the test in this time.

Check the properties in inspector if the game object PowerUp

* Did you notices there are 2 Assertions Components?
* One is to check that the Plane at the end is still equal to the original size,
* the other check that the player big is actually bigger that the player small at the end.

**No powerUp**

This test Has a Cube powerUp and a CapsuleNoPowerUp
The cube run the same script as the previous test but this capsule should not become bigger!

Check with the inspector the properties of the objects Test powerUp

* Did you notice that Succeed on assertions is check?
* Did you notice the time out? We should complete the test in this time

Check the properties in inspector if the game object CapsuleNoPowerUp

* Did you notices there are 1 Assertion Component?
* This Script check that the Capsule Scale did not change from the original size 1.


**Make the test fail **

Open the script called "makeitBig"
if you comment this line:


```
#!C#

other.transform.localScale = scale * 2;
```


run the test, 1 will fail, the other succeed. Why?


### Who do I talk to? ###

* If you need a fast answer check this video 

https://unity3d.com/learn/tutorials/topics/production/unity-test-tools

* Martin Reinoso (mreinoso@student.unimelb.edu.au)